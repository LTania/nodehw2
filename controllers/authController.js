const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.register = async (req, res) => {
  const {username, password} = req.body;
  if (username && password) {
    const user = await User.findOne({username});
    if (user) {
      res.status(400).json({message: 'User already exist'});
    } else {
      const user = new User({
        username,
        password: await bcrypt.hash(password, 10),
      });
      await user.save();

      res.status(200).json({message: 'User created'});
    }
  } else {
    res.status(400).json({message: 'Enter credentials'});
  }
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;
  if (!username && !password) {
    res.status(400).json({message: 'Enter credentials!'});
  } else {
    const user = await User.findOne({username});

    if (!user) {
      return res.status(400).json({message: `No user ${username}`});
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).json({message: `Wrong password`});
    }

    const token = jwt.sign({
      _id: user._id,
      username: user.username,
    }, JWT_SECRET);
    res.status(200).json({message: 'Success', jwt_token: token});
  }
};
