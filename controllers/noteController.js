const {Note} = require('../models/noteModel');
module.exports.getNotes = async (req, res) => {
  const {userId} = req;
  let {limit, offset} = req.query;
  offset = Number(offset);
  limit = Number(limit);
  const notes = await Note.find({userId}).skip(offset).limit(limit);
  return res.status(200).json({notes});
};

module.exports.addNote = async (req, res) => {
  const {userId} = req;
  const {text} = req.body;
  const note = new Note({text, userId});
  await note.save();
  return res.status(200).json({message: 'Note is added'});
};

module.exports.getNote = async (req, res) => {
  const {id} = req.params;
  const {userId} = req;
  const note = await Note.findOne({_id: id, userId});
  if (!note) {
    return res.status(400).json({message: 'Note is not found'});
  }
  return res.status(200).json({note});
};


module.exports.deleteNote = async (req, res) => {
  const {id} = req.params;
  const {userId} = req;
  const note = await Note.findOne({_id: id, userId}).exec();
  if (!note) {
    return res.status(400).json({message: 'Note is not found'});
  }
  await note.remove();
  return res.status(200).json({message: 'Note is deleted'});
};

module.exports.checkNote = async (req, res) => {
  const {id} = req.params;
  const {userId} = req;
  const note = await Note.findOne({_id: id, userId}).exec();
  if (!note) {
    return res.status(400).json({message: 'Note is not found'});
  }
  note.completed = !note.completed;
  await note.save();
  return res.status(200).json({message: 'Comleted status is changed'});
};

module.exports.updateNote = async (req, res) => {
  const {id} = req.params;
  const {userId} = req;
  const {text} = req.body;
  const note = await Note.findOne({_id: id, userId}).exec();
  if (!note) {
    return res.status(400).json({message: 'Note is not found'});
  }
  note.text = text;
  await note.save();
  return res.status(200).json({message: 'Text is changed'});
};
