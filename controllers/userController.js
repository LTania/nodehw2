const {User} = require('../models/userModel');

module.exports.deleteMe = async (req, res) => {
  const {userId} = req;
  const user = await User.findOne({_id: userId});
  if (!user) {
    return res.status(400).json({message: 'User doesnt exist'});
  }
  await User.deleteOne({_id: userId});
  return res.status(200).json({message: 'Success'});
};
