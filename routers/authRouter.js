const express = require('express');
const router = new express.Router();
const {register, login} = require('../controllers/authController');
const {asyncWrapper} = require('../helpers/asyncWrapper');
const {validateAuth} = require('../middlewares/validateAuth');

router.post('/register', asyncWrapper(validateAuth), asyncWrapper(register));
router.post('/login', asyncWrapper(login));

module.exports = router;
