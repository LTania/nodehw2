const express = require('express');
const router = new express.Router();
const {getNotes,
  addNote,
  getNote,
  checkNote,
  deleteNote,
  updateNote} = require('../controllers/noteController');
const {validateToken} = require('../middlewares/validateToken');
const {asyncWrapper} = require('../helpers/asyncWrapper');

router.get('/', validateToken, asyncWrapper(getNotes));
router.post('/', validateToken, asyncWrapper(addNote));
router.get('/:id', validateToken, asyncWrapper(getNote));
router.patch('/:id', validateToken, asyncWrapper(checkNote));
router.delete('/:id', validateToken, asyncWrapper(deleteNote));
router.put('/:id', validateToken, asyncWrapper(updateNote));

module.exports = router;
