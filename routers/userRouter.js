const express = require('express');
const router = new express.Router();
const {deleteMe} = require('../controllers/userController');
const {asyncWrapper} = require('../helpers/asyncWrapper');
const {validateToken} = require('../middlewares/validateToken');


router.delete('/', validateToken, asyncWrapper(deleteMe));

module.exports = router;
