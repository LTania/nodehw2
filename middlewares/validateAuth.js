const Joi = require('joi');

module.exports.validateAuth = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .required(),
    password: Joi.string()
        .required(),
  });
  await schema.validateAsync(req.body);
  next();
};
