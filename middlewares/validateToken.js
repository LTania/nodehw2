const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');

module.exports.validateToken = (req, res, next) => {
  const header = req.headers['authorization'];
  if (!header) {
    return res.status(401).json({
      message: `Add authorization please`,
    });
  }
  const [tokenType, token] = header.split(' ');
  console.log(tokenType);
  try {
    req.userId = jwt.verify(token, JWT_SECRET)._id;
    next();
  } catch (e) {
    return res.status(400).json({message: 'Invalid token'});
  }
};

