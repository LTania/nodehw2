const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const PORT = process.env.PORT || 8080;
const aRouter = require('./routers/authRouter');
const nRouter = require('./routers/noteRouter');
const uRouter = require('./routers/userRouter');

const app = express();
app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/auth', aRouter);
app.use('/api/notes', nRouter);
app.use('/api/users/me', uRouter);

const start = async () => {
  await mongoose.connect(
      'mongodb+srv://nodehw:nodehw@cluster0.zgh29.mongodb.net/someTask4DB?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );

  app.listen(PORT, () =>{
    console.log(`Server has been started on ${PORT}...`);
  });
};

start();


